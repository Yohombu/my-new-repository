//assignment 8

#include <stdio.h>

struct student {
	char fname[30]; //first name
	char subject[50];
	float marks;
};

int main(){
	int i,num;
	
	do{
		printf("Enter the number of students : ");
		scanf("%d",&num);
		if(num<5){
			printf("Minimum number is 5\n");
		}
	}while(num<5);
	
	struct student st[num];
	//get details
	
	for(i=0;i<num;i++){
		
		printf("Student %d\nEnter first name :",i+1);
		scanf("%s",st[i].fname);
		
		printf("Enter Subject : ");
		scanf("%s",st[i].subject);
		
		printf("Enter marks : ");
		scanf("%f",&st[i].marks);
		
		printf("\n");
	}
	//display infomrmation
	
	printf("\n\nDisplay information\n\n");
	
	for(i=0;i<num;i++){
		printf("Student %d\nFirst name %s\n",i+1,st[i].fname);
		printf("Subject : %s\n",st[i].subject);
		printf("Marks : %.2f\n",st[i].marks);
		printf("\n");
	}
	
	return 0;
}

