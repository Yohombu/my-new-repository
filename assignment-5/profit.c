#include <stdio.h>

int noattend(int price){
	return 120-(price-15)*20/5;
}

int revenue(int price){
	return price*noattend(price);
}

int cost(int price){
	return 3*noattend(price)+500;
}

int profit(int price){
	return revenue(price)-cost(price);
}

int main(){
	printf("Profits when ticket price is 10, 15, 20, 25, 30= %d, %d, %d, %d, %d\n",profit(10),profit(15),profit(20),profit(25),profit(30));
	
	printf("\nMax profit is 1260 So the best ticket price which maximize his profit is = Rs.25\n");
	return 0;
}
