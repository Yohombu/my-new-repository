#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]){ //Enter command line arguments : character and string
	int count=0;
    char s=argv[1][0];
    
    for(int i=2; i<argc; i++){
        for (int j = 0; j<strlen(argv[i]) ; j++)
            if(argv[i][j]==s) count++;
    }
    printf("The frequency of character %s is %d\n", argv[1],count);

    return 0; 
}
