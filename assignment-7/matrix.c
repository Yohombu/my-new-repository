#include <stdio.h>

#define ROWS 3
#define COLS 3

void read(int [ROWS][COLS]);

int main(){
	int m1[ROWS][COLS];
	int m2[ROWS][COLS];
	int m[ROWS][COLS];
	
	read(m1);
	read(m2);
	
	
	for(int i=0;i<ROWS;i++)
		for(int j=0; j<COLS;j++)
			m[i][j]=m1[i][j]+m2[i][j];
	printf("\nMatirx Addition:\n");
	for(int i=0;i<ROWS;i++){
		for(int j=0;j<COLS;j++)
			printf("%d ", m[i][j]);
		printf("\n");
	}
	
	for(int i=0;i<ROWS;i++){
		for(int j=0;j<COLS;j++){
			m[i][j]=0;
			for(int k=0;k<ROWS;k++){
				m[i][j] += m1[i][k] * m2[k][j];
			}
		}
	}
	printf("\nMatrix Multiplication:\n");
	for(int i=0;i<ROWS;i++){
		for(int j=0;j<COLS;j++){
			printf("%d ", m[i][j]);
		}
		printf("\n");
	}
	return 0;
}

void read(int m[ROWS][COLS]){
	printf("Enter %d X %d matrix\n",ROWS,COLS);
	for(int i=0; i<ROWS;i++)
		for(int j=0; j<COLS;j++)
			scanf("%d",&m[i][j]);
}


